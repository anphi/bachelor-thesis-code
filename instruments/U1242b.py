"""
Class for accessing the U1242b multimeter
"""
import re
import logging
import serial

logger = logging.getLogger("U1242b")


class U1242b:
    def __init__(self, port: str, baud: int = 9600, timeout: float = 3.0):
        try:
            self._dev = serial.Serial(port, baudrate=baud, timeout=timeout)
        except serial.SerialException as ex:
            logger.fatal(f"Couldn't open Serial device {port}: {ex}")
            raise

    def _write_data(self, data: bytes):
        if data[-2:] != b'\r\n': # add \r\n to string to terminate the command
            data += b"\r\n"
        self._dev.write(data)

    def _read_data(self, size: int = None) -> bytes:
        try:
            return self._dev.read_until(expected=b'\r\n', size=size)
        except serial.SerialTimeoutException:
            return b"*TIMEOUT"

    def get_id_string(self):
        self._write_data(b"*IDN?")
        return self._read_data(100)

    def get_current_value(self) -> str:
        self._write_data(b"FETC?")
        value = self._read_data(30).decode("ascii")

        # filter out any event notifiers that might pop up between two reads
        # but leave Errors
        # rot switch notifiers (*0 - *10)
        # button notifiers (*L)
        # see https://sigrok.org/wiki/Agilent_U12xxx_series#Event_Notifiers
        value = re.sub(r"\*(10|[\w^E])\r\n", "", value)

        value = value[:-2]  # slice \r\n
        if value[0] == "*":  # some error occurred
            if value[1] == 'E':
                raise SystemError("The previous command was invalid")
            if value[1] == 'B':
                raise SystemError("The battery is empty")
            if value[1] == 'I':
                raise SystemError("The probes are not connected properly")

        return value
