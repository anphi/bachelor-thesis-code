from enum import Enum
from RsInstrument import RsInstrument


class ZvlOperationMode(Enum):
    SPECTRUM_ANALYZER = 0
    NETWORK_ANALYZER = 1


class Zvl(RsInstrument):
    """
    Subclass for teh R&S ZVL Vector network analyzer
    implements only some basic functions
    """
    start_frequency = -1
    stop_frequency = -1
    span_frequency = -1
    center_frequency = -1
    sweep_points = -1

    def __init__(self, resource_name: str, id_query: bool = True, reset: bool = False, options: str = "",
                 direct_session=None):
        super().__init__(resource_name, id_query, reset, options, direct_session)
        self.write_str("FORM:BORD NORM")  # binary data is transferred Big endian (MSB bit first)
        self.write_str("FORM REAL, 32")  # binary data is transferred Big endian (MSB bit first)
        self.sweep_points = self.query_int("SWE:POINTS?")
        self._query_frequency_params()

    def set_operation_mode(self, mode: ZvlOperationMode):
        if mode == ZvlOperationMode.SPECTRUM_ANALYZER:
            self.write_str("INST:SEL SAN")
        elif mode == ZvlOperationMode.NETWORK_ANALYZER:
            self.write_str("INST:SEL NWA")

    def set_display_update_state(self, state: bool) -> None:
        """
        turns updating the display on or off for better performance
        :param state:
        :return:
        """
        if state:
            self.write_str('SYST:DISP:UPDATE ON')
        else:
            self.write_str('SYST:DISP:UPDATE OFF')

    def set_continous_sweep(self, state: bool) -> None:
        """
        turns on or off continous sweep mode
        :param state:
        :return:
        """
        if state:
            self.write_str('INIT:CONT ON')
        else:
            self.write_str('INIT:CONT OFF')

    def set_start_frequency(self, freq: str):
        """
        sets the sweep start frequency
        :param freq:
        :return:
        """
        self.write_str(f":FREQ:START {freq}")
        self._query_frequency_params()

    def set_stop_frequency(self, freq: str):
        """
        sets the sweep stop frequency
        :param freq:
        :return:
        """
        self.write_str(f":FREQ:STOP {freq}")
        self._query_frequency_params()

    def set_center_frequency(self, freq: str):
        """

        :param freq:
        :return:
        """
        self.write_str(f":FREQ:CENTER {freq}")
        self._query_frequency_params()

    def set_span_width(self, width: str):
        self.write_str(f":FREQ:SPAN {width}")
        self._query_frequency_params()

    def do_single_sweep(self, ch_n: int = 1):
        """
        does a single sweep and blocks until done
        :param ch_n: channel number to take a trace off
        :return:
        """
        if self.query_int("INIT:CONT?"):
            self.set_continous_sweep(False)
        self.write_str_with_opc(f"INIT{ch_n}")

    def get_ch_trace(self, ch_n: int = 1) -> list:
        """
        takes a sweep from the given channel with the parameters set in freq_start and freq_stop
        :param ch_n: channel number to query, default is 1
        :return:
        """
        self.do_single_sweep(ch_n)
        waveform = self.query_bin_or_ascii_float_list(f"TRAC? TRACE{ch_n}")
        return waveform

    def get_ch_trace_param(self, start: str, stop: str, ch_n: int) -> None:
        """
        sets start and stop frequency and performs
        :param start:
        :param stop:
        :param ch_n:
        :return:
        """
        self.write_str(f"FREQ:START {start}")
        self.write_str(f"FREQ:STOP {stop}")

    def do_place_max_marker(self) -> str:
        """
        places an marker in the current screen that searches for a maximum
        :return:
        """
        self.write_str_with_opc("CALC:MARK:FUNC:EXEC MAX")
        result = self.query_str_with_opc("CALC:MARK:FUNC:RES?")
        return result

    def get_sweep_points(self) -> int:
        return self.query_int("SWE:POINTS?")


    def _query_frequency_params(self):
        """
        reads start, stop, center, and span frequency
        :return:
        """
        self.start_frequency = self.query_int(":FREQ:START?")
        self.stop_frequency = self.query_int(":FREQ:STOP?")
        self.span_frequency = self.query_int(":FREQ:SPAN?")
        self.center_frequency = self.query_int(":FREQ:CENTER?")
