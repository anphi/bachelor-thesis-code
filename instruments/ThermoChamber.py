"""
class for accessing and controlling the vötsch thermo chamber
"""
import copy
from time import sleep

import serial


class ThermoChamberStatus:
    humidity_nominal = 0
    humidity_actual = 0
    temperature_nominal = 0
    temperature_actual = 0
    operation = False

    def __init__(self, temp_nom, temp_act, hum_nom, hum_act, running):
        self.humidity_nominal = float(hum_nom)
        self.humidity_actual = float(hum_act)
        self.temperature_nominal = float(temp_nom)
        self.temperature_actual = float(temp_act)
        self.operation = (running == '1')

    def __str__(self):
        prefix = "[RUN]" if self.operation else "[STOP]"
        return f"{prefix} | {self.temperature_actual}°C/{self.temperature_nominal}°C |" \
               f" {self.humidity_actual}%/{self.humidity_nominal}%"


class ThermoChamber:
    @staticmethod
    def str_to_status(ascii2_str: bytes) -> ThermoChamberStatus:
        assert len(ascii2_str) > 0, "the given string is empty"
        ascii2_str = ascii2_str.decode('ascii')
        item_list = ascii2_str.split(" ")
        bin_status = item_list[8]
        status = ThermoChamberStatus(
            temp_nom=item_list[0],
            temp_act=item_list[1],
            hum_nom=item_list[3],
            hum_act=item_list[4],
            running=bin_status[1]
        )
        return status

    def __init__(self, dev):
        """
        constructor
        :param dev serial device string
        """
        self._dev = serial.Serial(dev)
        self._status: ThermoChamberStatus = None
        self.query_status()

    def query_status(self):
        self._dev.read_all()
        self._dev.write(b'$01I\r')  # request current status
        sleep(.3)
        bin_str = self._dev.read_all()
        self._status = self.str_to_status(bin_str)

    @property
    def status(self):
        self.query_status()
        return copy.copy(self._status)

    def start_operation(self):
        cmd = self._cmd_to_ascii(operation=True)
        self._dev.write(cmd)
        self._status.operation = True
        sleep(.3)
        self.query_status()

    def stop_operation(self):
        cmd = self._cmd_to_ascii(operation=False)
        self._dev.write(cmd)
        self._status.operation = False
        sleep(.3)
        self.query_status()

    def set_new_temperature(self, temp):
        self.stop_operation()
        sleep(0.5)
        cmd = self._cmd_to_ascii(operation=True, temperature_nom=temp)
        self._dev.write(cmd)
        sleep(.3)
        self.query_status()

    def _cmd_to_ascii(self, operation: bool = None, temperature_nom: float = None, humidity_nom: float = None) -> bytes:
        if operation is None:
            operation = self._status.operation
        if temperature_nom is None:
            temperature_nom = self._status.temperature_nominal
        if humidity_nom is None:
            humidity_nom = self._status.humidity_nominal
        start_literal = '1' if operation else '0'

        valid_types = {float, int}
        assert type(start_literal) is str
        assert type(temperature_nom) in valid_types
        assert type(humidity_nom) in valid_types

        cmd_string = f"$01E {temperature_nom} {humidity_nom} 0{start_literal}{10 * '0'}\r"
        return cmd_string.encode('ascii')
