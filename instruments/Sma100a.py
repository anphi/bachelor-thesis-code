from RsInstrument import RsInstrument


class Sma100a(RsInstrument):
    power_limit = -10

    def set_power_limit(self, limit: int):
        """
        sets a local power limit in this object, not on the device
        :param limit:
        :return:
        """
        self.write_str(f":POW:LIM {limit}")
        self.power_limit = limit

    def set_output_state(self, state: bool) -> None:
        """
        enables or disables the RF output
        :param state: state to set the output to
        :return:
        """
        if state:
            self.write_str("OUTP ON")
        else:
            self.write_str("OUTP OFF")

    def set_output_power(self, power: int) -> None:
        """
        sets the output power to the given value after validating it isn't higher than the set limit
        in power_limit
        :param power: int value in dBm
        :return:
        """
        if power > self.power_limit:
            raise ValueError("The requested power level is higher than the currently set power limit")
        self.write_str(f"SOUR:POW:POW {power}")

    def set_output_frequency(self, frequency: str) -> None:
        """
        sets the main output frequency to the given value
        :param frequency: frequency string. Succeed number with unit
        example: '150 kHz'
        :return:
        """
        self.write_str(f"FREQ {frequency}")
