#Instruments

this folder contains the "drivers" and interface classes to the
various measurement devices used for data acquisition.

| File             | contents                                                                  |
|------------------|---------------------------------------------------------------------------|
| Rtl.py           | interface for sampling iq data out of the dongle with rtl-sdr lib         |
| Sma100a          | access class for controlling the R&S SMA100A frequency generator via VISA |
| ThermoChamber.py | class for accessing the Vötsch VCL4010 via Serial                         |
| U1242b.py        | class for accessing the U1242b Multimeter via USB Serial                  |
| Zvl.py           | class for accessing the R&S ZVL Vectoranalyzer via VISA                   |