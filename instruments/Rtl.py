"""
implements reading samples and converting it to a spectrum from an RTL-SDR-3 dongle
"""
from typing import Tuple

import numpy as np
from rtlsdr import RtlSdr
import logging

logger = logging.getLogger("RTL-SDR")


def read_spectrum(center: float, sample_rate: float = 2.048e6, n: int = 2 ** 15, ppm_corr: int = 1) -> Tuple[
    np.array, np.array]:
    """
    reads an spectrum from an RTL SDR dongle
    :param center: center frequency to tune to
    :param sample_rate: samplerate, defaults to 2.048 MHz since it is the fastest stable sample rate
    :param n: how many samples should be taken? default is 2^15 since the fft performs nice with these
    :param ppm_corr: ppm correction value for the radio, defaults to 1 ppm, since 0 returns an error
    :return: tuple with (frequencies, power)
    """
    # direct sampling
    # minimum: 0 Hz
    # maximum: ~ 29e6 Hz

    # normal
    # minimum 26.1e6
    # maximum: 1768.3e6 Hz

    if center <= 26.1e6:
        direct_sampling = True
        logger.info("direct sampling enabled")
    else:
        direct_sampling = False
    logger.info(f"Run with {center} Hz")

    sdr = RtlSdr()

    if direct_sampling:
        sdr.set_direct_sampling('q')
    else:
        sdr.set_direct_sampling(False)
    sdr.set_agc_mode(False)
    sdr.gain = 0
    sdr.sample_rate = sample_rate
    sdr.center_freq = center
    sdr.freq_correction = ppm_corr  # PPM, 0 yields error
    sdr.gain = 0

    samples = np.array(sdr.read_samples(n), dtype=np.complex)
    sdr.close()
    samples = samples - np.mean(samples)  # remove dc spike

    if direct_sampling:
        samples = samples.imag  # in direct sampling only q branch gets sampled, so strip the rest
    samples = samples * np.hamming(n)  # apply hamming window function to samples

    # perform fft and processing according to https://pysdr.org/content/sampling.html
    fft = np.fft.fft(samples)
    fft_mag = np.abs(fft)
    fft_norm = fft_mag / n
    fft_power = 10 * np.log10(fft_norm ** 2)
    fft_result = np.fft.fftshift(fft_power)

    f = np.arange(sample_rate / -2, sample_rate / 2, sample_rate / n)  # array containing all sampled frequencies
    f += center  # offset so it matches the actual frequencies instead of mixed down fft frequencies

    assert f.shape == fft_result.shape
    return f, fft_result


def sdr_sanity_check():
    sdr = RtlSdr()
    sdr.read_samples(512)
    sdr.close()
