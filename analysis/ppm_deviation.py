"""
for each frequency calculate the deviation to the desired maximum
"""
import json
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np

from measurements.constants import temperatures


def plot_dongle_ppm(folder: Path, outfile: Path, ylim: tuple = (-30, 30), t: str = "plot"):
    assert t == "plot" or t == "scatter", "type must be plot or scatter"
    plt.figure()
    cmap = plt.get_cmap("cool")
    for i, temp in enumerate(temperatures):
        with open(folder / f'{temp}deg.json', 'r') as f:
            data = json.load(f)[f"{temp}deg"]

        frequencies = []
        deviation = []

        for freq_str in data.keys():
            if freq_str == "temperature":
                continue
            freq = float(freq_str)
            peak_at = np.argmax(data[freq_str][1])
            peak_freq = data[freq_str][0][peak_at]
            frequencies.append(freq)
            deviation.append(peak_freq - freq)

        frequencies = np.array(frequencies)
        deviation = np.array(deviation)
        if t == "scatter":
            plt.scatter(frequencies, deviation / frequencies * 1e6, label=f"{temp} °C",
                        color=cmap(i / len(temperatures)), marker='_')
        else:
            plt.plot(frequencies, deviation / frequencies * 1e6, label=f"{temp} °C",
                     color=cmap(i / len(temperatures)))
    plt.legend()
    plt.grid()
    plt.ylim(ylim)
    # plt.title(folder.name)
    plt.xscale("log")
    plt.xlabel("Frequenz in Hz")
    plt.ylabel("Gemessene Abweichung in ppm")
    plt.savefig(outfile)
