import json
from pathlib import Path

import numpy as np
import numpy.ma as ma
from measurements.constants import temperatures


def calc_avg(folder: Path, minimum, maximum):
    means = []
    for i, temp in enumerate(temperatures):
        with open(folder / f'{temp}deg.json', 'r') as f:
            data = json.load(f)[f"{temp}deg"]

        frequencies = []
        deviation = []

        for freq_str in data.keys():
            if freq_str == "temperature":
                continue
            freq = float(freq_str)
            peak_at = np.argmax(data[freq_str][1])
            peak_freq = data[freq_str][0][peak_at]
            frequencies.append(freq)
            deviation.append(peak_freq - freq)

        frequencies = np.array(frequencies)
        deviation = np.array(deviation)

        ppm = deviation / frequencies * 1e6
        ppm = ma.masked_array(ppm)
        ppm = ma.masked_outside(ppm, minimum, maximum)

        means.append(np.mean(ppm))
        print(f"T={temp}°C: avg dev is {np.mean(ppm):.3f} PPM")
    print(f"Total mean: {np.mean(means):.2f} PPM")
    print(f"variance is {np.abs(np.max(means)) - np.abs(np.min(means)):.3f} PPM")


if __name__ == "__main__":
    print("=============== RTL3 ================")
    calc_avg(Path("../data/sweeps/rtl3"), -60, 60)
    print("")

    print("=============== RTL_TV ================")
    calc_avg(Path("../data/sweeps/rtl_tv"), -3000, 3000)
    print("")

    print("=============== RTL_green ================")
    calc_avg(Path("../data/sweeps/rtl_green"), -60, 60)
    print("")
