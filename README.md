# Measurement repository


This repo contains code and data taken during the bachelor's thesis
to analyse the accuracy of rtl sdr dongles.

| Folder      | Content                                                                   |
|-------------|---------------------------------------------------------------------------|
| analysis    | several scripts used for analysing and deriving metrics from the raw data |
| data        | raw data in json any npy format                                           |
| instruments | drivers for the used devices                                              |
| measurement | main routines for taking measurements and storing raw data                |
| plotting    | helpers for plotting raw data and/or derived data                         |