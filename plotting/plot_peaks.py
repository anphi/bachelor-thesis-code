from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt

from measurements.constants import powers

PATH = "../data/peaks"
PNG_PATH = Path("../../tex/figures/plots/")

# m, y of regression curve
# run plot_cable_att.py to obtain values
correction_curve = np.poly1d((-0.0005419875449893168, 0.13603539035788084))


def plot_freq(dev_str, freq):
    peaks = np.load(f"{PATH}/{dev_str}/{freq}_peaks.npy")

    power_slice = powers[:len(peaks)]
    peaks -= correction_curve(power_slice)  # subtract cable attenuation from measured values

    coefficents = np.polyfit(power_slice, peaks, 1)
    reg = np.poly1d(coefficents)

    non_lin = 1 - coefficents[0]
    offset = coefficents[1]

    plt.figure()
    plt.rc("font", size=12.5)
    plt.scatter(power_slice, peaks, marker="+", c="orange", label="gemessene Werte")
    plt.plot(powers, powers, label="Idealwerte")
    plt.plot(power_slice, reg(power_slice), linestyle="dashed")
    plt.text(-59, -70, f"non-linearity: {non_lin:.3f}")
    plt.text(-59, -75, f"offset: {offset:.3f}")
    plt.xlim([min(powers), max(powers) + 10])
    plt.ylim([min(powers), max(powers) + 10])
    plt.xlabel(f"Eingespeiste Leistung in dBm bei {freq} MHz")
    plt.ylabel("Gemessene Leistung in dB")
    plt.legend()
    plt.grid(True, "minor")
    plt.grid(True, "major")
    plt.savefig(PNG_PATH / f"{dev_str}_{freq}.png", bbox_inches='tight')


if __name__ == '__main__':
    for freq in [45, 100, 433, 800, 1200]:
        plot_freq("sdr3", freq)
        plot_freq("sdr_green", freq)
        plot_freq("sdr_tv", freq)
