from instruments.Rtl import read_spectrum, sdr_sanity_check
import matplotlib.pyplot as plt
import numpy as np

FREQ = 400.5e6

f, p = read_spectrum(FREQ)
maximum_index = np.argmax(p)

plt.figure()
plt.plot(f / 1e6, p)
plt.scatter(f[maximum_index] / 1e6, p[maximum_index], marker='x', c="orange")
plt.text(20, 0, f"{f[maximum_index]}")
plt.ylabel("Power in dBm")
plt.xlabel("Frequency in MHz")
plt.grid()
print(f"Maximum at {f[maximum_index] / 1e6:.2f} MHz: {p[maximum_index]:.2f} dBm")
plt.show()
