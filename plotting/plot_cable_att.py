import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

if __name__ == "__main__":
    df = pd.read_csv("../data/kabeldaempfung.csv", sep=";")
    f = df['freq']  # frequencies
    db = df["db"]  # attenuation in db
    min_pos = np.argmin(df['db'])

    f /= 1e6

    coefficents = np.polyfit(f, db, 1)
    m = coefficents[0]
    y = coefficents[1]
    reg = np.poly1d(coefficents)
    print(f"regression paramters: m={m} , y={y}")

    plt.figure()
    plt.plot(f, db, label="gemessene Kabeldämpfung")
    plt.plot(f, reg(f), label="Regressionsgerade")
    plt.scatter(f[min_pos], db[min_pos], c="orange", marker="x", label="Minimum")
    plt.text(810, -1.8, f"minimum: {db[min_pos]: .1f} dB @ {f[min_pos]: .1f} MHz")
    plt.text(810, -2.3, f"{m * 100:.3f} dB / 100 MHz")
    plt.grid()
    plt.ylim([-3, 2])
    plt.xlabel("Frequenz in MHz")
    plt.ylabel("Kabeldämpfung in dB")
    plt.legend()
    plt.savefig("../../tex/figures/plots/cable_attenuation.png")
