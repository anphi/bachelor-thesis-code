from analysis.ppm_deviation import plot_dongle_ppm
from pathlib import Path

# plot deviation of all 3 dongles directly to tex dir
PNG_PATH = Path("../../tex/figures/plots/")

if __name__ == "__main__":
    plot_dongle_ppm(Path("../data/sweeps/rtl_tv"), Path("/tmp/rtl_tv_ppm.png"), (-2500, 2500), "scatter")
    plot_dongle_ppm(Path("../data/sweeps/rtl3"), PNG_PATH / "rtl3_ppm.png", (-60, -30))
    plot_dongle_ppm(Path("../data/sweeps/rtl_tv"), PNG_PATH / "rtl_tv_ppm.png", (-2500, 2500), t="scatter")
    plot_dongle_ppm(Path("../data/sweeps/rtl_green"), PNG_PATH / "rtl_green_ppm.png", (-10, 10))
    print(f"[DONE] Successfully plotted to {PNG_PATH}")
