import json
import numpy as np
import matplotlib.pyplot as plt

f = open('../data/zvl_sweeps.json', 'r')
results = json.load(f)
f.close()
for k, v in results.items():
    plt.figure()
    plt.plot(v['x'], v['y'])
    plt.grid()
    plt.title(f"{k} Hz")
    plt.savefig(f"zvl_plot/{k}.png")
    plt.show()
