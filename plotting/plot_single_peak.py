import matplotlib.pyplot as plt
import numpy as np

from measurements.peak_measurement import initialise, measure_frequency
from measurements.constants import powers

initialise()
peaks, limit = measure_frequency(400e6, -45)

power_slice = powers[:len(peaks)]
peaks_delta = np.abs(power_slice - peaks)

plt.figure()
plt.scatter(power_slice, peaks_delta)
plt.xlabel("Power in dB")
plt.ylabel("absolute Abweichung")
plt.grid()
plt.show()

plt.figure()
plt.title("Limit Aufnahme")
plt.plot(limit[1] / 1e6, limit[2])
plt.ylabel("Power in dBm")
plt.xlabel("Frequency in MHz")
plt.grid()
plt.show()
