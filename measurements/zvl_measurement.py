import json
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
from instruments.Zvl import ZvlOperationMode, Zvl
from instruments.Sma100a import Sma100a
import constants

frequencies = constants.normal_frequencies
sweep_points = 10000
file_name = f'../data/zvl_sweep{datetime.now():%Y%m%d_%H:%M:%S}.json'

# SMA initialization
sma = Sma100a("TCPIP::192.168.0.2::hislip0::INSTR", True, True, "SelectVisa='rs'")
sma.reset()
sma.set_power_limit(-10)
sma.set_output_power(-20)
sma.set_output_state(False)

# ZVL initialization
zvl = Zvl("TCPIP::192.168.0.4::INSTR", True, True, "SelectVisa='rs'")
zvl.reset()
zvl.set_display_update_state(True)
zvl.set_operation_mode(ZvlOperationMode.SPECTRUM_ANALYZER)
zvl.set_continous_sweep(False)

results = {}

plt.Figure()

for f in frequencies:
    key = str(f)
    results[key] = {}
    sma.set_output_frequency(f)
    sma.set_output_state(True)
    zvl.set_center_frequency(f)
    zvl.set_span_width(.25 * f)
    y = zvl.get_ch_trace()
    x = np.linspace(zvl.start_frequency, zvl.stop_frequency, zvl.get_sweep_points(), dtype=np.int32)
    results[key]['x'] = x.tolist()
    results[key]['y'] = y

with open(file_name, 'w') as f:
    json.dump(results, f, indent=4)
