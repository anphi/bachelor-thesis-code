import logging
import sys
from time import sleep
import json
from datetime import datetime
import requests

import numpy as np
import tqdm
from instruments.Sma100a import Sma100a
from instruments.Rtl import read_spectrum, sdr_sanity_check
from instruments.U1242b import U1242b
from instruments.ThermoChamber import ThermoChamber

import constants

SMA_RES_STR = "TCPIP::192.168.0.2::hislip0::INSTR"
ZVL_RES_STR = "TCPIP::192.168.0.4::INSTR"
#TEMP_DEV = "/dev/ttyUSB0"
THERMO_DEV = "/dev/ttyUSB0"
TARGET_FOLDER = "../data/sweeps/"

logging.basicConfig(filename="measurement.log", level="INFO")
# in Hz logarithmically spaced
# direct sampling area from

# in Hz logarithmically spaced
# mixed down frequencies from 26.1 MHz to 1.7683 GHz

sma = Sma100a(SMA_RES_STR, True, True, "SelectVisa='rs'")
#multimeter = U1242b(TEMP_DEV)
thermo_chamber = ThermoChamber(THERMO_DEV)
results = {}


def send_message(msg: str):
    token = "645640857:AAE2sG6nQdWYzdmOI0WNtSHt8s4y0304Ohc"
    target = 559440734
    send_text = f'https://api.telegram.org/bot{token}/sendMessage?chat_id={target}&parse_mode=Markdown&text={msg}'
    try:
        response = requests.get(send_text)
        if response.status_code != 200:
            logging.error(f"Error while sending telegram message: {response.status_code}")
    except Exception as ex:
        logging.error(f"Error while sending telegram message: {ex}")


def initialise():
    sma.reset()
    sma.set_power_limit(-10)
    sma.set_output_power(-30)
    sma.set_output_state(False)


def sanity_check(res_dict):
    try:
        sdr_sanity_check()
        #float(multimeter.get_current_value())
        a = thermo_chamber.status
        assert not a.operation
        assert res_dict is not None
        send_message("Sanity check done, starting measurement")
    except Exception as ex:
        logging.error(f"Sanity check for SDR failed with: {ex}")
        send_message("Sanity check unsuccessful!")
        sys.exit(1)


def measure_frequencies(series_id: str, ppm: int = 1):
    temps = []
    if not results.get(series_id):
        results[series_id] = {}
    for freq in tqdm.tqdm(constants.normal_frequencies):
        sma.set_output_frequency(freq)
        sma.set_output_state(True)
        sleep(.6)  # let signal swing in
        f, db = read_spectrum(freq - 300e3, n=4096, ppm_corr=ppm)
        results[series_id][str(freq)] = [f.tolist(), db.tolist()]
        sma.set_output_state(False)
        try:
            #temps.append(float(multimeter.get_current_value()))
            temps.append(float(-1))
        except Exception as ex:
            logging.error(f"Error while fetching temperature: {ex}")
    results[series_id]['temperature'] = np.mean(temps)


def finalize():
    sma.set_output_state(False)


########################################
# BEGIN of measurements
########################################

def multi():
    global results
    try:
        initialise()
        sanity_check(results)

        for temperature in tqdm.tqdm(constants.temperatures):
            thermo_chamber.set_new_temperature(temperature)
            for i in tqdm.tqdm(range(50)):  # wait 50 min to let temperature stabilize
                #multimeter.get_current_value()
                sleep(60)
                pass
            #send_message(f"starting measurement {temperature}°, with temperature: {multimeter.get_current_value()}")
            send_message(f"starting measurement {temperature}°")
            measure_frequencies(f"{temperature}deg")
            with open(f"{TARGET_FOLDER}/rtl_old_{temperature}deg_{datetime.now():%Y.%m.%d %H:%M:%S}.json", 'w') as file:
                json.dump(results, file, indent=4)
            results = {}

        finalize()
        send_message("Measurement done, successfully")

    except Exception as ex:
        send_message(f"An critical error occured: {ex}")
    finally:
        thermo_chamber.stop_operation()


def single():
    sanity_check(results)
    initialise()
    measure_frequencies("60deg-45ppm", ppm=45)
    with open(f"{TARGET_FOLDER}/rtl_{60}deg45ppm_{datetime.now():%Y.%m.%d %H:%M:%S}.json", 'w') as file:
        json.dump(results, file, indent=4)


multi()
