import numpy as np

rtl_sample_rate = 2.048e6  # Hz
temperatures = [-10, 0, 10, 20, 30, 40, 50, 60]  # in °C
normal_frequencies = np.geomspace(26.1e6, 1.7683e9, num=100, dtype=np.int32)
powers = np.linspace(-15, -100, num=30)
