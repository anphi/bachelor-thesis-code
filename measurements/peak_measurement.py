import logging
from time import sleep
import requests

import numpy as np
from instruments.Sma100a import Sma100a
from instruments.Rtl import read_spectrum, sdr_sanity_check
from constants import powers

SMA_RES_STR = "TCPIP::192.168.0.2::hislip0::INSTR"
TARGET_FOLDER = "../data/amplitude"

logging.basicConfig(filename="measurement_amplitude.log", level="INFO")

sma = Sma100a(SMA_RES_STR, True, True, "SelectVisa='rs'")
sma.reset()
sma.set_output_state(False)


def send_message(msg: str):
    token = "645640857:AAE2sG6nQdWYzdmOI0WNtSHt8s4y0304Ohc"
    target = 559440734
    send_text = f'https://api.telegram.org/bot{token}/sendMessage?chat_id={target}&parse_mode=Markdown&text={msg}'
    try:
        response = requests.get(send_text)
        if response.status_code != 200:
            logging.error(f"Error while sending telegram message: {response.status_code}")
    except Exception as ex:
        logging.error(f"Error while sending telegram message: {ex}")


def initialise():
    sma.reset()
    sma.set_power_limit(-10)
    sma.set_output_power(-30)
    sma.set_output_state(False)


def measure_frequency(freq, ppm_corr):
    measured_peaks = []
    sma.set_output_frequency(freq)
    limit = None
    for power in powers:
        sma.set_output_power(power)
        sma.set_output_state(True)
        sleep(.2)
        f, fft = read_spectrum(freq - 300e3, ppm_corr=ppm_corr)
        peak_index = np.argmax(fft)
        logging.info(f"peak at {f[peak_index]} Hz: {fft[peak_index]} dB")
        error = freq - f[peak_index]
        if abs(error) > 600e3:
            logging.warning("frequency outside range, not valid")
            limit = f[peak_index], f, fft
            break
        measured_peaks.append(fft[peak_index])

    return measured_peaks, limit


if __name__ == '__main__':
    name = "sdr_tv"
    for i in [45e6, 100e6, 433e6, 800e6, 1200e6]:
        peaks, limit = measure_frequency(i, -1)
        np.save(f"../data/peaks/{name}_{i / 1e6:.0f}_peaks", peaks)
        np.save(f"../data/peaks/{name}_{i / 136:.0f}_limit", limit)
