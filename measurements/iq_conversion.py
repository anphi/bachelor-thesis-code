import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks

from rtlsdr import RtlSdr

sdr = RtlSdr()

# direct sampling
# minimum: 0 Hz
# maximum: ~ 29e6 Hz

# normal
# minimum 26.1e6
# maximum: 1768.3e6 Hz


sample_rate = 2.048e6  # Hz
center = 30.5e6  # Hz
N = 2 ** 15
slices = 1

# configure device
#sdr.set_direct_sampling('q')
sdr.set_agc_mode(True)
sdr.sample_rate = sample_rate
sdr.center_freq = center
sdr.freq_correction = 1  # PPM
sdr.gain = 0

samples = np.array(sdr.read_samples(N), dtype=np.complex)
sdr.close()
#samples = samples.imag

samples = samples.reshape((N//slices, slices))
samples = np.mean(samples, axis=1)
samples = samples * np.hamming(N//slices)


# samples = samples * np.hamming(N)
fft = np.fft.fft(samples)
fft_mag = np.abs(fft)
fft_norm = fft_mag / (N/slices)
fft_power = 10 * np.log10(fft_norm ** 2)
fft_result = np.fft.fftshift(fft_power)

peaks, _ = find_peaks(fft_result, height=-30, width=.05)

f = np.arange(sample_rate / -2, sample_rate / 2, sample_rate / (N/slices))
f += center
f /= 1e6
plt.figure()
plt.ylim([-120, 0])
plt.grid()

plt.plot(f, fft_result)
plt.scatter(f[peaks], fft_result[peaks], c='orange', marker='v')
    #plt.text(f[maximum], fft_result[maximum] + 10, f"{fft_result[maximum]:.2f} dBFS @ {f[maximum]} MHz",
    #     horizontalalignment="center")
plt.show()
