RsInstrument~=1.19.0.75
matplotlib~=3.3.4
numpy~=1.19.5
pyserial~=3.5
pyrtlsdr~=0.2.92
scipy~=1.5.4
tqdm~=4.62.3